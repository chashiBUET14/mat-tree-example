export class FoodNode {
  name: string;
  children?: FoodNode[];
  logoUrl: string;
}
