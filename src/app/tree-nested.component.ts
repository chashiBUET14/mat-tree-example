import { Component, OnInit } from '@angular/core';
import { FoodNode } from './foodNode.model';
import {ArrayDataSource} from '@angular/cdk/collections';
import {NestedTreeControl} from '@angular/cdk/tree';

const TREE_DATA: FoodNode[] = [
  {
    name: 'Fruit',
    children: [
      {name: 'Apple', logoUrl: 'assets/images/office_icon.png'},
      {name: 'Banana', logoUrl: 'assets/images/office_icon.png'},
      {name: 'Fruit loops', logoUrl: 'assets/images/office_icon.png'},
    ],
    logoUrl: 'assets/images/office_icon.png'
  }, {
    name: 'Vegetables',
    children: [
      {
        name: 'Green',
        children: [
          {name: 'Broccoli', logoUrl: 'assets/images/office_icon.png'},
          {name: 'Brussel sprouts', logoUrl: 'assets/images/office_icon.png'},
        ],
        logoUrl: 'assets/images/office_icon.png'
      }, {
        name: 'Orange',
        children: [
          {name: 'Pumpkins', logoUrl: 'assets/images/office_icon.png'},
          {name: 'Carrots', logoUrl: 'assets/images/office_icon.png'},
        ],
        logoUrl: 'assets/images/office_icon.png'
      },
    ],
    logoUrl: 'assets/images/office_icon.png'
  },
];


@Component({
  selector: 'app-tree-nested',
  templateUrl: './tree-nested.component.html',
  styleUrls: ['./tree-nested.component.css']
})
export class TreeNestedComponent implements OnInit {

  treeControl = new NestedTreeControl<FoodNode> (node => node.children);
  dataSource = new ArrayDataSource(TREE_DATA);

  hasChild = (_: number, node: FoodNode) => !!node.children && node.children.length > 0;
  constructor() { }

  ngOnInit() {
  }

}


