export class OfficeUnitPost {
  oid: string;
  displayOrder: number;
  postOid: string;
  businessOrder: number;
  levelOrder: number;
  parentOid: string;
  officeUnitOid: string;
}
