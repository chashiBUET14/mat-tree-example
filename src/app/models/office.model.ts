export class Office {
  oid: string;
  nameEn: string;
  nameBn: string;
  status: string;
  sortOrder: number;
  parentOid: string;
  rootOfficeOid: string;
  ministryOid: string;
  officeLayerOid: string;
  isRootOffice: string;
  codeEn: string;
  codeBn: string;
  addressEn: string;
  addressBn: string;
  web: string;
  email: string;
  logoUrl: string;
}
