export class OfficeUnit {
  oid: string;
  nameEn: string;
  nameBn: string;
  sortOrder: number;
}
