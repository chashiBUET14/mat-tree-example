import {Component, Input, OnInit} from '@angular/core';
import {FoodNode} from './foodNode.model';

@Component({
  selector: 'app-tree-node',
  templateUrl: './tree-node.component.html',
  styleUrls: ['./tree-node.component.css']
})
export class TreeNodeComponent implements OnInit {
  @Input() foodNode: FoodNode;
  constructor() { }

  ngOnInit() {
  }

}
